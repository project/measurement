<?php

namespace Drupal\measurement_ui\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\measurement\Entity\MeasurementUnitInterface;

/**
 * Provides the measurement formatter 'delete' form.
 */
class MeasurementFormatterDeleteForm extends ConfirmFormBase {

  /**
   * The measurement unit entity.
   *
   * @var \Drupal\measurement\Entity\MeasurementUnitInterface
   */
  protected $entity;

  /**
   * The measurement formatter plugin instance.
   *
   * @var \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'measurement_formatter_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MeasurementUnitInterface $measurement_unit = NULL, $measurement_formatter = NULL) {
    $this->entity = $this->prepareEntity($measurement_unit);
    $this->plugin = $this->preparePlugin($measurement_formatter);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->removeFormatter($this->plugin->getName());
    $this->entity->save();
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the %label formatter?', [
      '%label' => $this->plugin->getLabel(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('measurement_ui.formatter.overview_form', [
      'measurement_unit' => $this->entity->id(),
    ]);
  }

  /**
   * Prepares the entity object for the form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity object.
   */
  protected function prepareEntity(EntityInterface $entity) {
    return $entity;
  }

  /**
   * Prepares the plugin object for the form.
   *
   * @param string $measurement_formatter
   *   The measurement formatter identifier.
   *
   * @return \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface
   *   The measurement formatter plugin instance.
   */
  protected function preparePlugin($measurement_formatter) {
    return $this->entity->getFormatter($measurement_formatter);
  }

}
