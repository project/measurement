<?php

namespace Drupal\measurement_ui\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the measurement unit config entity form.
 */
class MeasurementUnitForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('The human-readable name.'),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine-readable name'),
      '#description' => $this->t('A unique machine-readable name. Can only contain lowercase letters, numbers, and underscores.'),
      '#default_value' => $this->entity->id(),
      '#disabled' => !$this->entity->isNew(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'replace_pattern' => '[^a-z0-9_]+',
      ],
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('The administrative description.'),
      '#default_value' => $this->entity->getDescription(),
    ];

    return $form;
  }

  /**
   * Determines if the machine name is available.
   *
   * @param string $machine_name
   *   The machine name.
   * @param array $element
   *   The form element.
   *
   * @return bool
   *   TRUE if the machine name is taken, FALSE otherwise.
   */
  public function exists($machine_name, array $element) {
    $entity_storage = $this->entityTypeManager->getStorage('measurement_unit');
    $entity = $entity_storage->load($machine_name);
    return !empty($entity);
  }

  /**
   * Gets the message to display to the user after saving the entity.
   *
   * @return string
   *   The translated string of the save message.
   */
  protected function getSaveMessage() {
    return $this->t('The @entity-type %label has been saved.', [
      '@entity-type' => $this->entity->getEntityType()->getLowercaseLabel(),
      '%label' => $this->entity->label(),
    ]);
  }

  /**
   * Gets the URL where the user should be redirected after saving.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  protected function getRedirectUrl() {
    return Url::fromRoute('entity.measurement_unit.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    drupal_set_message($this->getSaveMessage());
    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

}
