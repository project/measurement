<?php

namespace Drupal\measurement_ui\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\measurement\Entity\MeasurementUnitInterface;
use Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base measurement formatter form.
 */
abstract class MeasurementFormatterFormBase implements FormInterface, ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The plugin form factory.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * The measurement formatter plugin manager.
   *
   * @var \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface
   */
  protected $pluginManager;

  /**
   * The measurement formatter plugin instance.
   *
   * @var \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface
   */
  protected $plugin;

  /**
   * The measurement unit entity.
   *
   * @var \Drupal\measurement\Entity\MeasurementUnitInterface
   */
  protected $entity;

  /**
   * Constructs a new MeasurementFormatterFormBase.
   *
   * @param \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface $plugin_manager
   *   The measurement formatter plugin manager.
   * @param \Drupal\Core\Plugin\PluginFormFactoryInterface $plugin_form_factory
   *   The plugin form factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(MeasurementFormatterPluginManagerInterface $plugin_manager, PluginFormFactoryInterface $plugin_form_factory, TranslationInterface $string_translation) {
    $this->pluginManager = $plugin_manager;
    $this->pluginFormFactory = $plugin_form_factory;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.measurement.formatter'),
      $container->get('plugin_form.factory'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'measurement_formatter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MeasurementUnitInterface $measurement_unit = NULL, $measurement_formatter = NULL) {
    $this->entity = $this->prepareEntity($measurement_unit);
    $this->plugin = $this->preparePlugin($measurement_formatter);

    $form['#tree'] = TRUE;

    $form['formatter'] = [];
    $form['formatter'] = $this->buildPluginForm($form['formatter'], $form_state, $form);

    $form['actions'] = $this->buildActions($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->validatePluginForm($form['formatter'], $form_state, $form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->submitPluginForm($form['formatter'], $form_state, $form);
    $this->entity->setFormatter($this->plugin->getName(), $this->plugin);
    $this->entity->save();
    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

  /**
   * The plugin form builder.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The form structure.
   */
  protected function buildPluginForm(array $form, FormStateInterface $form_state, array &$complete_form) {
    if (($this->plugin instanceof PluginWithFormsInterface) && $this->plugin->hasFormClass('configure')) {
      $plugin_form_state = SubformState::createForSubform($form, $complete_form, $form_state);
      $plugin_form_object = $this->pluginFormFactory->createInstance($this->plugin, 'configure');
      $form = $plugin_form_object->buildConfigurationForm($form, $plugin_form_state);
    }

    return $form;
  }

  /**
   * The plugin form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  protected function validatePluginForm(array &$form, FormStateInterface $form_state, array &$complete_form) {
    if (($this->plugin instanceof PluginWithFormsInterface) && $this->plugin->hasFormClass('configure')) {
      $plugin_form_state = SubformState::createForSubform($form, $complete_form, $form_state);
      $plugin_form_object = $this->pluginFormFactory->createInstance($this->plugin, 'configure');
      $plugin_form_object->validateConfigurationForm($form, $plugin_form_state);
    }
  }

  /**
   * The plugin form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  protected function submitPluginForm(array &$form, FormStateInterface $form_state, array &$complete_form) {
    if (($this->plugin instanceof PluginWithFormsInterface) && $this->plugin->hasFormClass('configure')) {
      $plugin_form_state = SubformState::createForSubform($form, $complete_form, $form_state);
      $plugin_form_object = $this->pluginFormFactory->createInstance($this->plugin, 'configure');
      $plugin_form_object->submitConfigurationForm($form, $plugin_form_state);
    }
  }

  /**
   * Builds the form actions.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   A render array structure representing the form actions.
   */
  protected function buildActions(array $form, FormStateInterface $form_state) {
    $build['#type'] = 'actions';

    $actions = $this->getActions($form, $form_state);

    $count = 0;

    foreach (Element::children($actions) as $action) {
      $build[$action] = $actions[$action] + [
        '#weight' => ++$count * 5,
      ];
    }

    return $build;
  }

  /**
   * Gets the form actions.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   An associative array of form actions, keyed by action name.
   */
  protected function getActions(array $form, FormStateInterface $form_state) {
    $actions = [];

    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => ['::submitForm'],
      '#button_type' => 'primary',
    ];

    return $actions;
  }

  /**
   * Prepares the entity object for the form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity object.
   */
  protected function prepareEntity(EntityInterface $entity) {
    return $entity;
  }

  /**
   * Prepares the plugin object for the form.
   *
   * @param string $measurement_formatter
   *   The measurement formatter identifier.
   *
   * @return \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface
   *   The measurement formatter plugin instance.
   */
  protected function preparePlugin($measurement_formatter) {
    return $this->pluginManager->getInstance(['id' => $measurement_formatter]);
  }

  /**
   * Gets the URL where the user should be redirected after saving.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  protected function getRedirectUrl() {
    return Url::fromRoute('measurement_ui.formatter.overview_form', [
      'measurement_unit' => $this->entity->id(),
    ]);
  }

}
