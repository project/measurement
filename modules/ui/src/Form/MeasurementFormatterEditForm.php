<?php

namespace Drupal\measurement_ui\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the measurement formatter 'edit' form.
 */
class MeasurementFormatterEditForm extends MeasurementFormatterFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getActions(array $form, FormStateInterface $form_state) {
    $actions = parent::getActions($form, $form_state);

    $actions['delete'] = [
      '#type' => 'link',
      '#title' => $this->t('Delete'),
      '#url' => Url::fromRoute('measurement_ui.formatter.delete_form', [
        'measurement_unit' => $this->entity->id(),
        'measurement_formatter' => $this->plugin->getName(),
      ]),
      '#attributes' => ['class' => ['button', 'button--danger']],
    ];

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  protected function preparePlugin($measurement_formatter) {
    return $this->entity->getFormatter($measurement_formatter);
  }

}
