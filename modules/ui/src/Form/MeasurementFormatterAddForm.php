<?php

namespace Drupal\measurement_ui\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the measurement formatter 'add' form.
 */
class MeasurementFormatterAddForm extends MeasurementFormatterFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getActions(array $form, FormStateInterface $form_state) {
    $actions = parent::getActions($form, $form_state);

    $actions['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('measurement_ui.formatter.overview_form', [
        'measurement_unit' => $this->entity->id(),
      ]),
      '#attributes' => ['class' => ['button']],
    ];

    return $actions;
  }

}
