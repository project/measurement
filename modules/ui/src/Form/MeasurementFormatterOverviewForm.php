<?php

namespace Drupal\measurement_ui\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\measurement\Entity\MeasurementUnitInterface;
use Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the measurement formatter overview form.
 */
class MeasurementFormatterOverviewForm implements FormInterface, ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The measurement unit entity.
   *
   * @var \Drupal\measurement\Entity\MeasurementUnitInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'measurement_formatter_overview_form';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MeasurementUnitInterface $measurement_unit = NULL) {
    $this->entity = $measurement_unit;

    $form['#tree'] = TRUE;

    $form['formatters'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => $this->t('There are no formatters yet.'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'measurement-formatter-weight',
        ],
      ],
    ];

    foreach ($this->entity->getFormatters() as $formatter_id => $formatter) {
      $form['formatters'][$formatter_id] = $this->buildRow($formatter);
    }

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => ['::submitForm'],
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->entity->getFormatters() as $formatter_id => $formatter) {
      $weight = (int) $form_state->getValue(['formatters', $formatter_id, 'weight']);
      if ($weight !== $formatter->getWeight()) {
        $formatter->setWeight($weight);
      }
    }

    $this->entity->save();
  }

  /**
   * Builds the header row for the measurement formatter listing.
   *
   * @return array
   *   A render array structure of table header strings.
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['plugin'] = $this->t('Formatter');
    $header['summary'] = $this->t('Summary');
    $header['weight'] = $this->t('Weight');
    $header['operations'] = $this->t('Operations');

    return $header;
  }

  /**
   * Builds a row for a single entry in the measurement formatter listing.
   *
   * @param \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface $formatter
   *   The measurement formatter plugin instance.
   *
   * @return array
   *   A render array structure of measurement formatter information.
   */
  public function buildRow(MeasurementFormatterPluginInterface $formatter) {
    $row['#attributes']['class'][] = 'draggable';

    $row['#weight'] = $formatter->getWeight();

    $row['label']['#markup'] = $formatter->getLabel();

    $row['plugin']['#markup'] = $formatter->getPluginDefinition()['label'];

    $row['summary'] = [
      '#type' => 'inline_template',
      '#template' => '<div class="measurement-formatter-summary">{{ summary|safe_join("<br />") }}</div>',
      '#context' => [
        'summary' => $formatter->getSummary(),
      ],
    ];

    $row['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight for @label', [
        '@label' => $formatter->getLabel(),
      ]),
      '#title_display' => 'invisible',
      '#default_value' => $formatter->getWeight(),
      '#attributes' => ['class' => ['measurement-formatter-weight']],
    ];

    $row['operations'] = $this->buildOperations($formatter);

    return $row;
  }

  /**
   * Builds a renderable list of operation links for the measurement formatter.
   *
   * @param \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface $formatter
   *   The measurement formatter plugin instance.
   *
   * @return array
   *   A renderable array of operation links.
   */
  public function buildOperations(MeasurementFormatterPluginInterface $formatter) {
    return [
      '#type' => 'operations',
      '#links' => $this->getOperations($formatter),
    ];
  }

  /**
   * Provides an array of operation links for the measurement formatter.
   *
   * @param \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface $formatter
   *   The measurement formatter plugin instance.
   *
   * @return array
   *   An associative array of operation links.
   */
  public function getOperations(MeasurementFormatterPluginInterface $formatter) {
    $operations = [];

    $operations['edit'] = [
      'title' => $this->t('Edit'),
      'url' => Url::fromRoute('measurement_ui.formatter.edit_form', [
        'measurement_unit' => $this->entity->id(),
        'measurement_formatter' => $formatter->getName(),
      ]),
      'weight' => -10,
    ];

    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'url' => Url::fromRoute('measurement_ui.formatter.delete_form', [
        'measurement_unit' => $this->entity->id(),
        'measurement_formatter' => $formatter->getName(),
      ]),
      'weight' => 0,
    ];

    return $operations;
  }

}
