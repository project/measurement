<?php

namespace Drupal\measurement_ui\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\measurement\Entity\MeasurementUnitInterface;
use Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the measurement formatter controller.
 */
class MeasurementFormatterController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The measurement formatter plugin manager.
   *
   * @var \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Constructs a new MeasurementFormatterController.
   *
   * @param \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface $plugin_manager
   *   The measurement formatter plugin manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(MeasurementFormatterPluginManagerInterface $plugin_manager, TranslationInterface $string_translation) {
    $this->pluginManager = $plugin_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.measurement.formatter'),
      $container->get('string_translation')
    );
  }

  /**
   * Provides the measurement formatter 'add' page.
   *
   * @param \Drupal\measurement\Entity\MeasurementUnitInterface $measurement_unit
   *   The measurement unit entity.
   *
   * @return array
   *   A render array structure representing the page.
   */
  public function addPage(MeasurementUnitInterface $measurement_unit) {
    $build = [
      '#theme' => 'admin_block_content',
      '#content' => [],
    ];

    foreach ($this->pluginManager->getDefinitions() as $plugin_id => $plugin_definition) {
      $build['#content'][$plugin_id] = [
        'title' => $plugin_definition['label'],
        'description' => $plugin_definition['description'],
        'url' => Url::fromRoute('measurement_ui.formatter.add_form', [
          'measurement_unit' => $measurement_unit->id(),
          'measurement_formatter' => $plugin_id,
        ]),
      ];
    }

    return $build;
  }

  /**
   * Provides the measurement formatter 'add' form title.
   *
   * @param string $measurement_formatter
   *   The measurement formatter plugin ID.
   *
   * @return string
   *   The form title.
   */
  public function addFormTitle($measurement_formatter) {
    if ($this->pluginManager->hasDefinition($measurement_formatter)) {
      return $this->t('Add %label formatter', [
        '%label' => $this->pluginManager->getDefinition($measurement_formatter)['label'],
      ]);
    }
  }

  /**
   * Provides the measurement formatter 'edit' form title.
   *
   * @param \Drupal\measurement\Entity\MeasurementUnitInterface $measurement_unit
   *   The measurement unit entity.
   * @param string $measurement_formatter
   *   The measurement formatter instance ID.
   *
   * @return string
   *   The form title.
   */
  public function editFormTitle(MeasurementUnitInterface $measurement_unit, $measurement_formatter) {
    return $this->t('Edit %label formatter', [
      '%label' => $measurement_unit->getFormatter($measurement_formatter)->getLabel(),
    ]);
  }

}
