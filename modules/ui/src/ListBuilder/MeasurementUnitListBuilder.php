<?php

namespace Drupal\measurement_ui\ListBuilder;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides the measurement unit config entity list builder.
 */
class MeasurementUnitListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    $header['description'] = $this->t('Description');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['name']['data'] = $entity->label();
    $row['description']['data'] = $entity->getDescription();

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('There are no measurement units yet.');

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    $operations['formatters'] = [
      'title' => $this->t('Manage formatters'),
      'url' => Url::fromRoute('measurement_ui.formatter.overview_form', [
        'measurement_unit' => $entity->id(),
      ]),
      'weight' => -20,
    ];

    return $operations;
  }

}
