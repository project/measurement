<?php

namespace Drupal\measurement_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base class for numeric measurement field types.
 */
abstract class NumericMeasurementFieldItemBase extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array_merge(parent::defaultStorageSettings(), [
      'measurement_type' => '',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['measurement_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Measurement type'),
      '#description' => $this->t('The type of measurement to store.'),
      '#default_value' => $this->getSetting('measurement_type'),
      '#disabled' => $has_data,
      '#required' => TRUE,
      '#options' => [],
    ];

    $entity_type_manager = \Drupal::service('entity_type.manager');
    $entity_storage = $entity_type_manager->getStorage('measurement_type');

    foreach ($entity_storage->loadMultiple() as $measurement_type_id => $measurement_type) {
      $element['measurement_type']['#options'][$measurement_type_id] = $measurement_type->label();
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return array_merge(parent::defaultFieldSettings(), [
      'units' => [],
      'min' => '',
      'max' => '',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);

    $element['units'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Measurement units'),
      '#description' => $this->t('The units of measurement to allow. Leave blank to allow all.'),
      '#default_value' => $this->getSetting('units'),
      '#options' => [],
    ];

    $entity_type_manager = \Drupal::service('entity_type.manager');
    $entity_storage = $entity_type_manager->getStorage('measurement_unit');

    foreach ($entity_storage->loadMultiple() as $unit_id => $unit) {
      $element['units']['#options'][$unit_id] = $unit->label();
    }

    $element['min'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum'),
      '#description' => $this->t('The minimum value that should be allowed in this field. Leave blank for no minimum.'),
      '#default_value' => $this->getSetting('min'),
    ];

    $element['max'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum'),
      '#description' => $this->t('The maximum value that should be allowed in this field. Leave blank for no maximum.'),
      '#default_value' => $this->getSetting('max'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->value) && (string) $this->value !== '0';
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = $this->getTypedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();
    $settings = $this->getSettings();

    if (!empty($settings['min'])) {
      $constraints[] = $constraint_manager->create('ComplexData', [
        'value' => [
          'Range' => [
            'min' => $settings['min'],
            'minMessage' => $this->t('%name: the value may be no less than %min.', [
              '%name' => $this->getFieldDefinition()->getLabel(),
              '%min' => $settings['min'],
            ]),
          ],
        ],
      ]);
    }

    if (!empty($settings['max'])) {
      $constraints[] = $constraint_manager->create('ComplexData', [
        'value' => [
          'Range' => [
            'max' => $settings['max'],
            'maxMessage' => $this->t('%name: the value may be no greater than %max.', [
              '%name' => $this->getFieldDefinition()->getLabel(),
              '%max' => $settings['max'],
            ]),
          ],
        ],
      ]);
    }

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function calculateDependencies(FieldDefinitionInterface $field_definition) {
    $dependencies = [];
    $units = $field_definition->getSetting('units');

    if (!empty($units)) {
      foreach ($units as $unit_id) {
        $dependencies['config'][] = 'measurement.unit.' . $unit_id;
      }
    }

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public static function calculateStorageDependencies(FieldStorageDefinitionInterface $field_definition) {
    $dependencies = [];
    $measurement_type = $field_definition->getSetting('measurement_type');

    if (!empty($measurement_type)) {
      $dependencies['config'][] = 'measurement.type.' . $measurement_type;
    }

    return $dependencies;
  }

}
