<?php

namespace Drupal\measurement_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\measurement\Entity\MeasurementUnit;

/**
 * Defines the decimal measurement field type.
 *
 * @FieldType(
 *   id = "measurement_decimal",
 *   label = @Translation("Measurement (decimal)"),
 *   description = @Translation("Stores a measurement in the database as a decimal."),
 *   category = @Translation("Measurement"),
 *   default_widget = "measurement_decimal",
 *   default_formatter = "measurement:decimal"
 * )
 */
class DecimalMeasurementFieldItem extends NumericMeasurementFieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array_merge(parent::defaultStorageSettings(), [
      'precision' => 10,
      'scale' => 2,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Decimal value'))
      ->setRequired(TRUE);

    $properties['unit'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Unit'));

    $properties['measurement'] = DataDefinition::create('measurement')
      ->setLabel(new TranslatableMarkup('Measurement'))
      ->setComputed(TRUE)
      ->setReadOnly(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'numeric',
          'precision' => $field_definition->getSetting('precision'),
          'scale' => $field_definition->getSetting('scale'),
        ],
        'unit' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['precision'] = [
      '#type' => 'number',
      '#title' => $this->t('Precision'),
      '#description' => $this->t('The total number of digits to store in the database, including those to the right of the decimal.'),
      '#default_value' => $this->getSetting('precision'),
      '#disabled' => $has_data,
      '#min' => 10,
      '#max' => 32,
    ];

    $element['scale'] = [
      '#type' => 'number',
      '#title' => $this->t('Scale', [], ['context' => 'decimal places']),
      '#description' => $this->t('The number of digits to the right of the decimal.'),
      '#default_value' => $this->getSetting('scale'),
      '#disabled' => $has_data,
      '#min' => 0,
      '#max' => 10,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if (isset($values) && !is_array($values)) {
      $this->set('measurement', $values, $notify);
    }
    else {
      parent::setValue($values, FALSE);

      if (isset($values['measurement'])) {
        $this->onChange('measurement', FALSE);
      }
      else {
        $this->writePropertyValue('measurement', [
          'magnitude' => isset($values['value']) ? $values['value'] : 0,
          'unit' => isset($values['unit']) ? $values['unit'] : NULL,
        ]);
      }

      // Notify the parent if necessary.
      if ($notify && $this->parent) {
        $this->parent->onChange($this->getName());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {
    switch ($property_name) {
      case 'measurement':
        // Get the measurement property.
        $measurement = $this->get('measurement');

        // Get the measurement unit.
        $unit = $measurement->getUnit();

        // Update the mirrored values.
        $this->writePropertyValue('value', $measurement->getMagnitude());
        $this->writePropertyValue('unit', $unit ? $unit->id() : NULL);

        break;

      case 'value':
        // Get the measurement property.
        $measurement = $this->get('measurement');

        // Update the measurement object.
        $measurement->setMagnitude($this->value, FALSE);

        break;

      case 'unit':
        // Get the measurement property.
        $measurement = $this->get('measurement');

        // Get the unit object.
        $unit = isset($this->unit) ? MeasurementUnit::load($this->unit) : NULL;

        // Update the measurement object.
        $measurement->setUnit($unit, FALSE);

        break;
    }

    parent::onChange($property_name, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = $this->getTypedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();

    $constraints[] = $constraint_manager->create('ComplexData', [
      'value' => [
        'Regex' => [
          'pattern' => '/^[+-]?((\d+(\.\d*)?)|(\.\d+))$/i',
        ],
      ],
    ]);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);

    $element['min']['#step'] = pow(0.1, $this->getSetting('scale'));
    $element['max']['#step'] = pow(0.1, $this->getSetting('scale'));

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $this->value = round($this->value, $this->getSetting('scale'));
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $settings = $field_definition->getSettings();
    $precision = $settings['precision'] ?: 10;
    $scale = $settings['scale'] ?: 2;

    // The minimum number you can get with 3 digits is -1 * (10^3 - 1).
    $min = is_numeric($settings['min']) ?: -pow(10, ($precision - $scale)) + 1;

    // The maximum number you can get with 3 digits is 10^3 - 1 --> 999.
    $max = is_numeric($settings['max']) ?: pow(10, ($precision - $scale)) - 1;

    // Get the number of decimal digits for the minimum limit.
    $min_decimal_digits = static::getDecimalDigits($min);

    // Get the number of decimal digits for the maximum limit.
    $max_decimal_digits = static::getDecimalDigits($max);

    // Get the maximum number of decimal digits of the limits.
    $decimal_digits = max($min_decimal_digits, $max_decimal_digits);

    $scale = rand($decimal_digits, $scale);

    $random_decimal = $min + mt_rand() / mt_getrandmax() * ($max - $min);

    $values['value'] = static::truncateDecimal($random_decimal, $scale);

    return $values;
  }

  /**
   * Helper method to get the number of decimal digits out of a decimal number.
   *
   * @param int $decimal
   *   The number to calculate the number of decimals digits from.
   *
   * @return int
   *   The number of decimal digits.
   */
  protected static function getDecimalDigits($decimal) {
    $digits = 0;

    while ($decimal - round($decimal)) {
      $decimal *= 10;
      $digits++;
    }

    return $digits;
  }

  /**
   * Helper method to truncate a decimal number to a given number of decimals.
   *
   * @param float $decimal
   *   The number to truncate.
   * @param int $num
   *   The number of digits the output will have.
   *
   * @return float
   *   The trucated number.
   */
  protected static function truncateDecimal($decimal, $num) {
    return floor($decimal * pow(10, $num)) / pow(10, $num);
  }

}
