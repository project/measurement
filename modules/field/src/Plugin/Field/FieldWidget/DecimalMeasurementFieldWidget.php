<?php

namespace Drupal\measurement_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the decimal measurement field widget.
 *
 * @FieldWidget(
 *   id = "measurement_decimal",
 *   label = @Translation("Measurement (decimal)"),
 *   field_types = {
 *     "measurement_decimal",
 *   }
 * )
 */
class DecimalMeasurementFieldWidget extends NumericMeasurementFieldWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $units = $this->getFieldSetting('units');

    $element['measurement'] = [
      '#type' => 'measurement',
      '#title' => $element['#title'],
      '#description' => $element['#description'],
      '#required' => $element['#required'],
      '#default_value' => $items[$delta]->measurement,
      '#magnitude_min' => $this->getFieldSetting('min'),
      '#magnitude_max' => $this->getFieldSetting('max'),
      '#magnitude_min_scale' => $this->getFieldSetting('scale'),
      '#magnitude_max_scale' => $this->getFieldSetting('scale'),
      '#magnitude_placeholder' => $this->getSetting('placeholder'),
      '#unit_names' => array_intersect(array_keys($units), $units),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return $element['measurement'];
  }

}
