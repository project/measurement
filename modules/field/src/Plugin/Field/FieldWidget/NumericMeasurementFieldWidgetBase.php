<?php

namespace Drupal\measurement_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base class for numeric measurement field widgets.
 */
abstract class NumericMeasurementFieldWidgetBase extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array_merge(parent::defaultSettings(), [
      'placeholder' => '',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
      '#default_value' => $this->getSetting('placeholder'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $placeholder = $this->getSetting('placeholder');

    if (!empty($placeholder)) {
      $summary[] = $this->t('Placeholder: @placeholder', [
        '@placeholder' => $placeholder,
      ]);
    }

    return $summary;
  }

}
