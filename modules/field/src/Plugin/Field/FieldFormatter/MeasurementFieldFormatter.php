<?php

namespace Drupal\measurement_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\AllowedTagsXssTrait;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\measurement\Type\Measurement;
use Drupal\measurement\Type\MeasurementInterface;
use Drupal\measurement\Utility\MeasurementFormatterInterface;

/**
 * Defines the measurement field formatter.
 *
 * @FieldFormatter(
 *   id = "measurement",
 *   label = @Translation("Measurement"),
 *   field_types = {
 *     "measurement_decimal",
 *   },
 *   deriver = "\Drupal\measurement_field\Plugin\Derivative\MeasurementFieldFormatterDeriver",
 * )
 */
class MeasurementFieldFormatter extends FormatterBase implements MeasurementFormatterInterface {

  use AllowedTagsXssTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->settings['formatter_id'] = $plugin_definition['formatter_id'];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array_merge(parent::defaultSettings(), [
      'formatter_id' => NULL,
      'formatter_configuration' => [],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    if ($formatter_form = $this->getFormatterPluginForm()) {
      $element['#element_validate'] = [[$this, 'settingsFormValidate']];

      $element['formatter_configuration'] = [
        '#type' => 'container',
      ];

      $formatter_form_state = SubformState::createForSubform($element['formatter_configuration'], $form, $form_state);

      $element['formatter_configuration'] = $formatter_form->buildConfigurationForm($element['formatter_configuration'], $formatter_form_state);

      $element['formatter_configuration']['label']['#type'] = 'hidden';
      $element['formatter_configuration']['label']['#default_value'] = 'Plugin';

      $element['formatter_configuration']['name']['#type'] = 'hidden';
      $element['formatter_configuration']['name']['#default_value'] = 'plugin';
    }

    return $element;
  }

  /**
   * Validates the settings form.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param array $form
   *   The form array.
   */
  public function settingsFormValidate($element, FormStateInterface $form_state, $form) {
    if ($formatter_form = $this->getFormatterPluginForm()) {
      $formatter_form_state = SubformState::createForSubform($element['formatter_configuration'], $form, $form_state);
      $formatter_form->validateConfigurationForm($element['formatter_configuration'], $formatter_form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    if ($formatter = $this->getFormatterPlugin()) {
      return $formatter->getSummary();
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function format(MeasurementInterface $measurement, $format = '', $options = []) {
    if ($formatter = $this->getFormatterPlugin()) {
      return $formatter->format($measurement, $format, $options);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $entity_storage = $entity_type_manager->getStorage('measurement_unit');

    foreach ($items as $delta => $item) {
      if (!empty($item->unit)) {
        $unit = $entity_storage->load($item->unit);
      }

      $measurement = new Measurement($item->value, $unit);
      $output = $this->format($measurement, $this->getSetting('formatter_id'));

      if (isset($item->_attributes)) {
        $item->_attributes += [
          'value' => $item->value,
          'unit' => $item->unit,
        ];
      }

      $elements[$delta]['#markup'] = $output;
    }

    return $elements;
  }

  /**
   * Gets the measurement formatter plugin.
   *
   * @return \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface|null
   *   The measurement formatter plugin, or NULL if it does not exist.
   */
  protected function getFormatterPlugin() {
    $plugin_manager = \Drupal::service('plugin.manager.measurement.formatter');

    if ($plugin = $plugin_manager->getInstance(['id' => $this->getSetting('formatter_id')])) {
      $plugin->setConfiguration($this->getSetting('formatter_configuration'));

      return $plugin;
    }
  }

  /**
   * Gets the measurement formatter plugin form.
   *
   * @return \Drupal\Core\Plugin\PluginFormInterface|null
   *   The measurement formatter plugin form, or NULL if there is no form.
   */
  protected function getFormatterPluginForm() {
    if (($plugin = $this->getFormatterPlugin()) && ($plugin instanceof PluginWithFormsInterface) && $plugin->hasFormClass('configure')) {
      $plugin_form_factory = \Drupal::service('plugin_form.factory');

      return $plugin_form_factory->createInstance($plugin, 'configure');
    }
  }

}
