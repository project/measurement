<?php

namespace Drupal\measurement_field\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the measurement field formatter deriver.
 */
class MeasurementFieldFormatterDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The measurement formatter plugin manager.
   *
   * @var \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Constructs a new MeasurementFieldFormatterDeriver.
   *
   * @param \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface $plugin_manager
   *   The measurement formatter plugin manager.
   */
  public function __construct(MeasurementFormatterPluginManagerInterface $plugin_manager) {
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.measurement.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->pluginManager->getDefinitions() as $plugin_id => $plugin_definition) {
      $this->derivatives[$plugin_id] = array_merge($base_plugin_definition, [
        'label' => $plugin_definition['label'],
        'description' => $plugin_definition['description'],
        'formatter_id' => $plugin_id,
      ]);
    }

    return $this->derivatives;
  }

}
