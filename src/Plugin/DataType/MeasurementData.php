<?php

namespace Drupal\measurement\Plugin\DataType;

use Drupal\Core\TypedData\TypedData;
use Drupal\measurement\Entity\MeasurementUnit;
use Drupal\measurement\Entity\MeasurementUnitInterface;
use Drupal\measurement\Type\Measurement;
use Drupal\measurement\Type\MeasurementInterface;

/**
 * Defines the measurement data type.
 *
 * @DataType(
 *   id = "measurement",
 *   label = @Translation("Measurement"),
 *   description = @Translation("The measurement data type.")
 * )
 */
class MeasurementData extends TypedData implements MeasurementInterface {

  /**
   * The measurement object.
   *
   * @var \Drupal\measurement\Type\MeasurementInterface
   */
  protected $measurement;

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    return $this->measurement;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    // Support passing measurement objects.
    if (is_object($value) && ($value instanceof MeasurementInterface)) {
      $this->setMeasurement(new Measurement($value->getMagnitude(), $value->getUnit()), $notify);
    }
    // Support passing arrays.
    elseif (is_array($value)) {
      $magnitude = isset($value['magnitude']) ? $value['magnitude'] : 0;
      $unit = isset($value['unit']) ? $value['unit'] : NULL;
      if ($unit && !($unit instanceof MeasurementUnitInterface)) {
        $unit = MeasurementUnit::load($unit);
      }
      $this->setMeasurement(new Measurement($magnitude, $unit), $notify);
    }
    // Support passing numeric values.
    elseif (is_numeric($value)) {
      $this->setMeasurement(new Measurement((float) $value), $notify);
    }
    elseif (!isset($value)) {
      $this->setMeasurement(new Measurement(0), $notify);
    }
    // Throw an error on invalid values.
    else {
      throw new \InvalidArgumentException('Invalid value given. The value must either be an existing measurement or a number.');
    }
  }

  /**
   * Gets the measurement object.
   *
   * @return \Drupal\measurement\Type\MeasurementInterface
   *   The measurement object.
   */
  public function getMeasurement() {
    if (!$this->measurement) {
      $this->measurement = new Measurement(0);
    }

    return $this->measurement;
  }

  /**
   * Sets the measurement object.
   *
   * @param \Drupal\measurement\Type\MeasurementInterface $measurement
   *   The measurement object.
   * @param bool $notify
   *   (optional) Whether to notify the parent. Defaults to TRUE.
   */
  public function setMeasurement(MeasurementInterface $measurement, $notify = TRUE) {
    $this->measurement = $measurement;

    // Notify the parent of any changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMagnitude() {
    return $this->getMeasurement()->getMagnitude();
  }

  /**
   * {@inheritdoc}
   */
  public function setMagnitude($magnitude) {
    $this->getMeasurement()->setMagnitude($magnitude);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUnit() {
    return $this->getMeasurement()->getUnit();
  }

  /**
   * {@inheritdoc}
   */
  public function setUnit(MeasurementUnitInterface $unit) {
    $this->getMeasurement()->setUnit($unit);
  }

}
