<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\measurement\Utility\MeasurementFormatterInterface;

/**
 * Defines a common interface for measurement formatter plugins.
 */
interface MeasurementFormatterPluginInterface extends ConfigurablePluginInterface, PluginInspectionInterface, DerivativeInspectionInterface, MeasurementFormatterInterface {

  /**
   * Gets the name of the formatter.
   *
   * @return string
   *   The formatter name.
   */
  public function getName();

  /**
   * Sets the name of the formatter.
   *
   * @param string $name
   *   The formatter name.
   *
   * @return $this
   */
  public function setName($name);

  /**
   * Gets the label of the formatter.
   *
   * @return string
   *   The formatter label.
   */
  public function getLabel();

  /**
   * Sets the label of the formatter.
   *
   * @param string $label
   *   The formatter label.
   *
   * @return $this
   */
  public function setLabel($label);

  /**
   * Gets the weight of the formatter.
   *
   * @return int
   *   The formatter weight.
   */
  public function getWeight();

  /**
   * Sets the weight of the formatter.
   *
   * @param int $weight
   *   The formatter weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Gets the summary of formatting options.
   *
   * @return string[]
   *   A short summary of formatting options.
   */
  public function getSummary();

  /**
   * Checks if a setting exists.
   *
   * @param string $name
   *   The setting name.
   *
   * @return bool
   *   TRUE if the setting exists, FALSE otherwise.
   */
  public function hasSetting($name);

  /**
   * Gets the value of a setting, or a default value if absent.
   *
   * @param string $name
   *   The setting name.
   * @param mixed $default
   *   (optional) The default value.
   *
   * @return mixed
   *   The setting value.
   */
  public function getSetting($name, $default = NULL);

  /**
   * Sets the value of a setting.
   *
   * @param string $name
   *   The setting name.
   * @param mixed $value
   *   The setting value.
   *
   * @return $this
   */
  public function setSetting($name, $value);

  /**
   * Unsets the value of a setting.
   *
   * @param string $name
   *   The setting name.
   *
   * @return $this
   */
  public function unsetSetting($name);

  /**
   * Gets the settings, including defaults for missing settings.
   *
   * @return array
   *   An associative array of settings, keyed by setting names.
   */
  public function getSettings();

  /**
   * Sets the settings.
   *
   * @param array $settings
   *   An associative array of settings, keyed by setting names. Missing
   *   settings will be assigned their default values.
   *
   * @return $this
   */
  public function setSettings(array $settings);

  /**
   * Provides the default settings.
   *
   * @return array
   *   An associative array of default settings, keyed by setting names.
   */
  public function defaultSettings();

}
