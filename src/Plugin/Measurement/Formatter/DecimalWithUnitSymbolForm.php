<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the decimal with unit symbol measurement formatter form.
 */
class DecimalWithUnitSymbolForm extends DecimalForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['settings']['symbol'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Symbol'),
      '#description' => $this->t('The symbol for the unit.'),
      '#default_value' => $this->plugin->getSetting('symbol'),
      '#size' => 4,
      '#weight' => 30,
    ];

    $form['settings']['symbol_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Symbol position'),
      '#description' => $this->t('The position of the symbol.'),
      '#default_value' => $this->plugin->getSetting('symbol_position'),
      '#options' => [
        DecimalWithUnitSymbol::SYMBOL_LEFT => $this->t('Left'),
        DecimalWithUnitSymbol::SYMBOL_RIGHT => $this->t('Right'),
      ],
      '#weight' => 40,
    ];

    return $form;
  }

}
