<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * Defines the measurement formatter plugin collection.
 */
class MeasurementFormatterPluginCollection extends DefaultLazyPluginCollection {

  /**
   * {@inheritdoc}
   */
  protected $pluginKey = 'id';

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface
   *   The measurement formatter plugin instance.
   */
  public function &get($instance_id) {
    return parent::get($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID) {
    $a_weight = $this->get($aID)->getWeight();
    $b_weight = $this->get($bID)->getWeight();

    if ($a_weight == $b_weight) {
      return 0;
    }

    return ($a_weight < $b_weight) ? -1 : 1;
  }

}
