<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for measurement formatter plugin forms.
 */
abstract class MeasurementFormatterPluginFormBase extends PluginFormBase implements ContainerInjectionInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;

    $form['#process'] = [[$this, 'processConfigurationForm']];

    $form['id'] = [
      '#type' => 'hidden',
      '#value' => $this->plugin->getPluginId(),
    ];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('The human-readable name.'),
      '#default_value' => $this->plugin->getLabel(),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#weight' => 0,
    ];

    $form['name'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine-readable name'),
      '#description' => $this->t('A unique machine-readable name. Can only contain lowercase letters, numbers, and underscores.'),
      '#default_value' => $this->plugin->getName(),
      '#disabled' => !empty($this->plugin->getName()),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'replace_pattern' => '[^a-z0-9_]+',
      ],
      '#weight' => 10,
    ];

    $form['weight'] = [
      '#type' => 'hidden',
      '#value' => $this->plugin->getWeight(),
      '#weight' => 20,
    ];

    $form['settings'] = [
      '#type' => 'container',
      '#weight' => 30,
    ];

    return $form;
  }

  /**
   * Processes the configuration form.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $complete_form
   *   The complete form structure.
   */
  public function processConfigurationForm(array $form, FormStateInterface $form_state, array &$complete_form) {
    // Set the machine name source to the label form field.
    $form['name']['#machine_name']['source'] = array_merge($form['#array_parents'], ['label']);

    // Copy form values to the plugin in case the form has been rebuilt.
    $this->copyFormValuesToPlugin($this->plugin, $form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // No validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Copy form values to the plugin to ensure configuration is saved.
    $this->copyFormValuesToPlugin($this->plugin, $form, $form_state);
  }

  /**
   * Copies form values to the given plugin.
   *
   * @param \Drupal\Component\Plugin\PluginInspectionInterface $plugin
   *   The plugin instance.
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function copyFormValuesToPlugin(PluginInspectionInterface $plugin, array $form, FormStateInterface $form_state) {
    // Ensure that the plugin is configurable.
    if ($plugin instanceof ConfigurablePluginInterface) {
      // Deep merge the form state values as to not remove any defaults that do
      // not have a stored value.
      $plugin_configuration = NestedArray::mergeDeep($plugin->getConfiguration(), $form_state->getValues());

      // Set the plugin configuration.
      $plugin->setConfiguration($plugin_configuration);
    }
  }

  /**
   * Determines if the machine name is available.
   *
   * @param string $machine_name
   *   The machine name.
   * @param array $element
   *   The form element.
   *
   * @return bool
   *   TRUE if the machine name is taken, FALSE otherwise.
   */
  public function exists($machine_name, array $element) {
    // There is currently no way to detect which plugin collection the plugin
    // belongs to so the machine name cannot yet be validated.
    return FALSE;
  }

}
