<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\measurement\Type\MeasurementInterface;

/**
 * Defines the decimal with unit text measurement formatter.
 *
 * @MeasurementFormatter(
 *   id = "decimal_with_unit_text",
 *   label = @Translation("Number (decimal, with unit text)"),
 *   description = @Translation("Formats a measurement as a decimal with unit text."),
 *   forms = {
 *     "configure" = "Drupal\measurement\Plugin\Measurement\Formatter\DecimalWithUnitTextForm",
 *   }
 * )
 */
class DecimalWithUnitText extends Decimal {

  /**
   * An integer represeting the left alignment of the text.
   */
  const TEXT_LEFT = 0;

  /**
   * An integer representing the right alignment of the text.
   */
  const TEXT_RIGHT = 1;

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return array_merge(parent::defaultSettings(), [
      'text' => '',
      'text_plural' => '',
      'text_position' => static::TEXT_RIGHT,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function format(MeasurementInterface $measurement, $format = '', $options = []) {
    $options = array_merge($this->getSettings(), $options);
    $number = parent::format($measurement, $format, $options);

    if (!empty($options['text'])) {
      $text = $options['text'];

      if (!empty($options['text_plural'])) {
        $text = (string) $this->formatPlural($measurement->getMagnitude(), $text, $options['text_plural']);
      }

      switch ($options['text_position']) {
        case static::TEXT_LEFT:
          $number = $text . ' ' . $number;
          break;

        case static::TEXT_RIGHT:
          $number = $number . ' ' . $text;
          break;
      }
    }

    return $number;
  }

}
