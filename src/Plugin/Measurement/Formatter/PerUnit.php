<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\measurement\Type\Measurement;
use Drupal\measurement\Type\MeasurementInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the per-unit measurement formatter.
 *
 * @MeasurementFormatter(
 *   id = "per_unit",
 *   label = @Translation("Per-unit"),
 *   description = @Translation("Formats a measurement according to its unit."),
 *   forms = {
 *     "configure" = "Drupal\measurement\Plugin\Measurement\Formatter\PerUnitForm",
 *   }
 * )
 */
class PerUnit extends MeasurementFormatterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return array_merge(parent::defaultSettings(), [
      'unit_mapping' => [],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function format(MeasurementInterface $measurement, $format = '', $options = []) {
    if ($unit = $measurement->getUnit()) {
      $unit_name = $unit->id();
      $unit_mapping = $this->getSetting('unit_mapping');

      if (isset($unit_mapping[$unit_name]) && !empty($unit_mapping[$unit_name])) {
        if ($formatter = $unit->getFormatter($unit_mapping[$unit_name])) {
          return $formatter->format($measurement, $unit_mapping[$unit_name], $options);
        }
      }
    }

    return $measurement->getMagnitude();
  }

}
