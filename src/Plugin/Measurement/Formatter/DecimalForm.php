<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the decimal measurement formatter form.
 */
class DecimalForm extends MeasurementFormatterPluginFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['settings']['scale'] = [
      '#type' => 'number',
      '#title' => $this->t('Scale', [], ['context' => 'decimal places']),
      '#description' => $this->t('The number of digits to the right of the decimal.'),
      '#default_value' => $this->plugin->getSetting('scale'),
      '#min' => 0,
      '#max' => 10,
      '#weight' => 0,
    ];

    $form['settings']['decimal_separator'] = [
      '#type' => 'select',
      '#title' => $this->t('Decimal marker'),
      '#description' => $this->t('The character used to separate the decimals.'),
      '#default_value' => $this->plugin->getSetting('decimal_separator'),
      '#options' => [
        '.' => $this->t('Decimal point'),
        ',' => $this->t('Comma'),
      ],
      '#weight' => 10,
    ];

    $form['settings']['thousand_separator'] = [
      '#type' => 'select',
      '#title' => $this->t('Thousand marker'),
      '#description' => $this->t('The character used to separate the thousands.'),
      '#default_value' => $this->plugin->getSetting('thousand_separator'),
      '#options' => [
        ''  => $this->t('- None -'),
        '.' => $this->t('Decimal point'),
        ',' => $this->t('Comma'),
        ' ' => $this->t('Space'),
        chr(8201) => $this->t('Thin space'),
        "'" => $this->t('Apostrophe'),
      ],
      '#weight' => 20,
    ];

    return $form;
  }

}
