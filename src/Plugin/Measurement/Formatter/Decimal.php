<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\measurement\Type\Measurement;
use Drupal\measurement\Type\MeasurementInterface;

/**
 * Defines the decimal measurement formatter.
 *
 * @MeasurementFormatter(
 *   id = "decimal",
 *   label = @Translation("Number (decimal)"),
 *   description = @Translation("Formats a measurement as a decimal."),
 *   forms = {
 *     "configure" = "Drupal\measurement\Plugin\Measurement\Formatter\DecimalForm",
 *   }
 * )
 */
class Decimal extends MeasurementFormatterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return array_merge(parent::defaultSettings(), [
      'scale' => 2,
      'decimal_separator' => '.',
      'thousand_separator' => ',',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function format(MeasurementInterface $measurement, $format = '', $options = []) {
    $options = array_merge($this->getSettings(), $options);
    $number = number_format($measurement->getMagnitude(), $options['scale'], $options['decimal_separator'], $options['thousand_separator']);

    return $number;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      $this->format(new Measurement(1234.1234567890), $this->getPluginId()),
    ];
  }

}
