<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\measurement\Annotation\MeasurementFormatter;

/**
 * Manages the discovery of measurement formatter plugins.
 */
class MeasurementFormatterPluginManager extends DefaultPluginManager implements MeasurementFormatterPluginManagerInterface {

  /**
   * Constructs a new MeasurementFormatterPluginManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Measurement/Formatter', $namespaces, $module_handler, MeasurementFormatterPluginInterface::class, MeasurementFormatter::class);

    $this->alterInfo('measurement_formatter_plugin_info');
    $this->setCacheBackend($cache_backend, 'measurement_formatter_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    return $this->createInstance($options['id']);
  }

}
