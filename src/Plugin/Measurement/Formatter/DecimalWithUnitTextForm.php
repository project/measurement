<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the decimal with unit text measurement formatter form.
 */
class DecimalWithUnitTextForm extends DecimalForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['settings']['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text'),
      '#description' => $this->t('The text for the unit.'),
      '#default_value' => $this->plugin->getSetting('text'),
      '#size' => 32,
      '#weight' => 30,
    ];

    $form['settings']['text_plural'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text (plural)'),
      '#description' => $this->t('The plural text for the unit.'),
      '#default_value' => $this->plugin->getSetting('text_plural'),
      '#size' => 32,
      '#weight' => 40,
    ];

    $form['settings']['text_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Text position'),
      '#description' => $this->t('The position of the text.'),
      '#default_value' => $this->plugin->getSetting('text_position'),
      '#options' => [
        DecimalWithUnitText::TEXT_LEFT => $this->t('Left'),
        DecimalWithUnitText::TEXT_RIGHT => $this->t('Right'),
      ],
      '#weight' => 50,
    ];

    return $form;
  }

}
