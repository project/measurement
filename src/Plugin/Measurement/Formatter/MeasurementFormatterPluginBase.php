<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\measurement\Type\MeasurementInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for measurement formatter plugins.
 */
abstract class MeasurementFormatterPluginBase implements MeasurementFormatterPluginInterface, PluginWithFormsInterface, ContainerFactoryPluginInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;
  use PluginWithFormsTrait;

  /**
   * A string which is used to separate base plugin IDs from the derivative ID.
   */
  const DERIVATIVE_SEPARATOR = ':';

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * The plugin definition.
   *
   * @var array
   */
  protected $pluginDefinition;

  /**
   * The formatter name.
   *
   * @var array
   */
  protected $name;

  /**
   * The formatter label.
   *
   * @var array
   */
  protected $label;

  /**
   * The formatter weight.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * The formatter settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * Constructs a new MeasurementFormatterPluginBase.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $this->pluginId = $plugin_id;
    $this->pluginDefinition = $plugin_definition;

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseId() {
    $plugin_id = $this->getPluginId();

    if (strpos($plugin_id, static::DERIVATIVE_SEPARATOR)) {
      list($plugin_id) = explode(static::DERIVATIVE_SEPARATOR, $plugin_id, 2);
    }

    return $plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeId() {
    $plugin_id = $this->getPluginId();
    $derivative_id = NULL;

    if (strpos($plugin_id, static::DERIVATIVE_SEPARATOR)) {
      list(, $derivative_id) = explode(static::DERIVATIVE_SEPARATOR, $plugin_id, 2);
    }

    return $derivative_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    return $this->pluginDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [
      'id' => $this->getPluginId(),
      'name' => $this->getName(),
      'label' => $this->getLabel(),
      'weight' => $this->getWeight(),
      'settings' => $this->getSettings(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $configuration = $configuration + $this->defaultConfiguration();

    $this->setName($configuration['name']);
    $this->setLabel($configuration['label']);
    $this->setWeight($configuration['weight']);
    $this->setSettings($configuration['settings']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'name' => '',
      'label' => '',
      'weight' => 0,
      'settings' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function format(MeasurementInterface $measurement, $format = '', $options = []);

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->name = $name;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel($label) {
    $this->label = $label;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function hasSetting($name) {
    return array_key_exists($name, $this->settings);
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting($name, $default = NULL) {
    if ($this->hasSetting($name)) {
      return $this->settings[$name];
    }

    return $default;
  }

  /**
   * {@inheritdoc}
   */
  public function setSetting($name, $value) {
    $this->settings[$name] = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetSetting($name) {
    unset($this->settings[$name]);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $settings) {
    $this->settings = $settings + $this->defaultSettings();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return [];
  }

}
