<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the per-unit measurement formatter form.
 */
class PerUnitForm extends MeasurementFormatterPluginFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new PerUnitForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['settings']['unit_mapping'] = [
      '#type' => 'container',
    ];

    $unit_storage = $this->entityTypeManager->getStorage('measurement_unit');

    foreach ($unit_storage->loadMultiple() as $unit_id => $unit) {
      $formatters = $unit->getFormatters();
      $unit_mapping = $this->plugin->getSetting('unit_mapping');

      $form['settings']['unit_mapping'][$unit_id] = [
        '#type' => 'select',
        '#title' => $unit->label(),
        '#options' => [],
        '#default_value' => isset($unit_mapping[$unit_id]) ? $unit_mapping[$unit_id] : '',
        '#empty_option' => $this->t('- None -'),
        '#emtpty_value' => '',
        '#disabled' => empty($formatters),
      ];

      foreach ($formatters as $formatter) {
        $form['settings']['unit_mapping'][$unit_id]['#options'][$formatter->getName()] = $formatter->getLabel();
      }
    }

    return $form;
  }

}
