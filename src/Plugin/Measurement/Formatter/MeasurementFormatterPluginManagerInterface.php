<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Defines an interface for the discovery of measurement formatter plugins.
 */
interface MeasurementFormatterPluginManagerInterface extends PluginManagerInterface {

}
