<?php

namespace Drupal\measurement\Plugin\Measurement\Formatter;

use Drupal\measurement\Type\MeasurementInterface;

/**
 * Defines the decimal with unit symbol measurement formatter.
 *
 * @MeasurementFormatter(
 *   id = "decimal_with_unit_symbol",
 *   label = @Translation("Number (decimal, with unit symbol)"),
 *   description = @Translation("Formats a measurement as a decimal with a unit symbol."),
 *   forms = {
 *     "configure" = "Drupal\measurement\Plugin\Measurement\Formatter\DecimalWithUnitSymbolForm",
 *   }
 * )
 */
class DecimalWithUnitSymbol extends Decimal {

  /**
   * An integer represeting the left alignment of the symbol.
   */
  const SYMBOL_LEFT = 0;

  /**
   * An integer representing the right alignment of the symbol.
   */
  const SYMBOL_RIGHT = 1;

  /**
   * {@inheritdoc}
   */
  public function defaultSettings() {
    return array_merge(parent::defaultSettings(), [
      'symbol' => '',
      'symbol_position' => static::SYMBOL_RIGHT,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function format(MeasurementInterface $measurement, $format = '', $options = []) {
    $options = array_merge($this->getSettings(), $options);
    $number = parent::format($measurement, $format, $options);

    if (!empty($options['symbol'])) {
      switch ($options['symbol_position']) {
        case static::SYMBOL_LEFT:
          $number = $options['symbol'] . $number;
          break;

        case static::SYMBOL_RIGHT:
          $number = $number . $options['symbol'];
          break;
      }
    }

    return $number;
  }

}
