<?php

namespace Drupal\measurement\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the measurement type config entity type.
 *
 * @ConfigEntityType(
 *   id = "measurement_type",
 *   label = @Translation("Measurement type"),
 *   admin_permission = "administer measurement types",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_prefix = "type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class MeasurementType extends ConfigEntityBase implements MeasurementTypeInterface {

  /**
   * The machine-readable name of the entity.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the entity.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the entity.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;

    return $this;
  }

}
