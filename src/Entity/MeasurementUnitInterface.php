<?php

namespace Drupal\measurement\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;
use Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface;

/**
 * Defines a common interface for measurement unit config entities.
 */
interface MeasurementUnitInterface extends ConfigEntityInterface, EntityDescriptionInterface {

  /**
   * Checks for a measurement formatter plugin with the given instance ID.
   *
   * @param string $instance_id
   *   The measurement formatter plugin instance ID.
   *
   * @return bool
   *   TRUE if the formatter exists, FALSE otherwise.
   */
  public function hasFormatter($instance_id);

  /**
   * Gets a measurement formatter plugin by the given instance ID.
   *
   * @param string $instance_id
   *   The measurement formatter plugin instance ID.
   *
   * @return \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface|null
   *   The measurement formatter plugin instance, if it exists.
   */
  public function getFormatter($instance_id);

  /**
   * Sets a measurement formatter plugin for the given instance ID.
   *
   * @param string $instance_id
   *   The measurement formatter plugin instance ID.
   * @param \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface $formatter
   *   The measurement formatter plugin instance.
   *
   * @return $this
   */
  public function setFormatter($instance_id, MeasurementFormatterPluginInterface $formatter);

  /**
   * Removes a measurement formatter plugin with the given instance ID.
   *
   * @param string $instance_id
   *   The measurement formatter plugin instance ID.
   *
   * @return \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface|null
   *   The removed measurement formatter plugin instance, if it exists.
   */
  public function removeFormatter($instance_id);

  /**
   * Gets the measurement formatter plugins.
   *
   * @return \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface[]
   *   An associative array of measurement formatter plugins, keyed by instance
   *   ID.
   */
  public function getFormatters();

}
