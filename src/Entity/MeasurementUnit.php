<?php

namespace Drupal\measurement\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginCollection;
use Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginInterface;

/**
 * Defines the measurement unit config entity type.
 *
 * @ConfigEntityType(
 *   id = "measurement_unit",
 *   label = @Translation("Measurement unit"),
 *   admin_permission = "administer measurement units",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_prefix = "unit",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "formatters",
 *   }
 * )
 */
class MeasurementUnit extends ConfigEntityBase implements MeasurementUnitInterface, EntityWithPluginCollectionInterface {

  /**
   * The machine-readable name of the entity.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the entity.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the entity.
   *
   * @var string
   */
  protected $description;

  /**
   * The measurement formatter plugin configuration.
   *
   * @var array
   */
  protected $formatters = [];

  /**
   * The measurement formatter plugin collection.
   *
   * @var \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginCollection
   */
  protected $formatterCollection;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasFormatter($instance_id) {
    return $this->getFormatterPluginCollection()->has($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatter($instance_id) {
    if ($this->hasFormatter($instance_id)) {
      return $this->getFormatterPluginCollection()->get($instance_id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setFormatter($instance_id, MeasurementFormatterPluginInterface $formatter) {
    $this->getFormatterPluginCollection()->set($instance_id, $formatter);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeFormatter($instance_id) {
    if ($formatter = $this->getFormatter($instance_id)) {
      $this->getFormatterPluginCollection()->removeInstanceId($instance_id);

      return $formatter;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatters() {
    $formatters = [];

    foreach ($this->getFormatterPluginCollection() as $formatter_id => $formatter) {
      $formatters[$formatter_id] = $formatter;
    }

    return $formatters;
  }

  /**
   * Gets the measurement formatter plugin manager.
   *
   * @return \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface
   *   The measurement formatter plugin manager.
   */
  protected function getFormatterPluginManager() {
    return \Drupal::service('plugin.manager.measurement.formatter');
  }

  /**
   * Encapsulates the creation of the measurement formatter plugin collection.
   *
   * @return \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginCollection
   *   The measurement formatter plugin collection.
   */
  protected function getFormatterPluginCollection() {
    if (!$this->formatterCollection) {
      $this->formatterCollection = new MeasurementFormatterPluginCollection(
        $this->getFormatterPluginManager(),
        $this->formatters
      );

      $this->formatterCollection->sort();
    }

    return $this->formatterCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'formatters' => $this->getFormatterPluginCollection(),
    ];
  }

}
