<?php

namespace Drupal\measurement\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines a common interface for measurement type config entities.
 */
interface MeasurementTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
