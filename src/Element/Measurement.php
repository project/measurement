<?php

namespace Drupal\measurement\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\measurement\Entity\MeasurementUnitInterface;
use Drupal\measurement\Type\MeasurementInterface;

/**
 * Defines the measurement form element.
 *
 * Usage example:
 * @code
 * $form['height'] = [
 *   '#type' => 'measurement',
 *   '#title' => new TranslatableMarkup('Height'),
 *   '#description' => new TranslatableMarkup('The height element.'),
 *   '#default_value' => new Measurement(50, MeasurementUnit::load('metre')),
 *   '#unit_names' => ['metre', 'kilometre'],
 *   '#size' => 128,
 *   '#required' => TRUE,
 * ];
 * @endcode
 *
 * @FormElement("measurement")
 */
class Measurement extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#input' => TRUE,
      '#size' => 20,
      '#default_value' => NULL,
      '#magnitude_step' => NULL,
      '#magnitude_min' => NULL,
      '#magnitude_max' => NULL,
      '#magnitude_min_scale' => NULL,
      '#magnitude_max_scale' => NULL,
      '#magnitude_placeholder' => '',
      '#unit_names' => [],
      '#pre_render' => [
        [static::class, 'preRenderGroup'],
      ],
      '#process' => [
        [static::class, 'processElement'],
        [static::class, 'processAutocomplete'],
        [static::class, 'processAjaxForm'],
        [static::class, 'processPattern'],
        [static::class, 'processGroup'],
      ],
      '#theme_wrappers' => ['form_element'],
      '#attached' => [
        'library' => ['measurement/admin'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    return NULL;
  }

  /**
   * Processes the measurement form element.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param array $complete_form
   *   The complete form array.
   */
  public static function processElement(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $element['#attributes']['class'][] = 'form-type-measurement';
    $element['#tree'] = TRUE;

    $element['magnitude'] = [
      '#type' => 'number',
      '#title' => new TranslatableMarkup('Magnitude'),
      '#title_display' => 'invisible',
      '#size' => $element['#size'],
      '#step' => 'any',
      '#placeholder' => $element['#magnitude_placeholder'],
    ];

    $element['unit'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Unit'),
      '#title_display' => 'invisible',
      '#options' => [],
    ];

    // Set the minimum magnitude if specified.
    if (isset($element['#magnitude_min'])) {
      $element['magnitude']['#min'] = $element['#magnitude_min'];
    }

    // Set the maximum magnitude if specified.
    if (isset($element['#magnitude_max'])) {
      $element['magnitude']['#max'] = $element['#magnitude_max'];
    }

    // Set the magnitude step if specified.
    if (isset($element['#magnitude_step'])) {
      $element['magnitude']['#step'] = $element['#magnitude_step'];
    }
    // Otherwise calculate the step from the maximum scale.
    elseif (isset($element['#magnitude_max_scale'])) {
      $element['magnitude']['#step'] = pow(0.1, $element['#magnitude_max_scale']);
    }

    $entity_type_manager = \Drupal::service('entity_type.manager');
    $entity_storage = $entity_type_manager->getStorage('measurement_unit');
    $allowed_units = $element['#unit_names'];

    // Add the unit selection options.
    foreach ($entity_storage->loadMultiple() as $unit_id => $unit_config) {
      if (empty($allowed_units) || in_array($unit_id, $allowed_units)) {
        $element['unit']['#options'][$unit_id] = $unit_config->label();
      }
    }

    // Set the default value.
    if ($measurement = $element['#default_value']) {
      if ($measurement instanceof MeasurementInterface) {
        $magnitude = (string) $measurement->getMagnitude();
        $unit_name = NULL;
        if ($unit = $measurement->getUnit()) {
          $unit_name = $unit->id();
        }
      }
      elseif (is_array($measurement)) {
        $magnitude = isset($measurement['magnitude']) ? (string) $measurement['magnitude'] : '0';
        $unit_name = isset($measurement['unit']) ? $measurement['unit'] : NULL;
        if ($unit_name instanceof MeasurementUnitInterface) {
          $unit_name = $unit_name->id();
        }
      }

      if (!empty($magnitude)) {
        // Trim decimal places to the minimum scale.
        if (($index = strpos($magnitude, '.')) != FALSE) {
          $decimals = strlen(substr($magnitude, $index + 1));
          $min_scale = isset($element['#magnitude_min_scale']) ? $element['#magnitude_min_scale'] : 0;

          while (($decimals > $min_scale) && substr($magnitude, -1) == '0') {
            $magnitude = substr($magnitude, 0, -1);
            $decimals = $decimals - 1;
          }

          $magnitude = rtrim($magnitude, '.');
        }

        $element['magnitude']['#default_value'] = $magnitude;
      }

      if (!empty($unit_name)) {
        $element['unit']['#default_value'] = $unit_name;
      }
    }

    return $element;
  }

}
