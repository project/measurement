<?php

namespace Drupal\measurement\Utility;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface;
use Drupal\measurement\Type\MeasurementInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the measurement formatter service.
 */
class MeasurementFormatter implements MeasurementFormatterInterface, ContainerInjectionInterface {

  use DependencySerializationTrait;

  /**
   * The measurement formatter plugin manager.
   *
   * @var \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Constructs a new MeasurementFormatter.
   *
   * @param \Drupal\measurement\Plugin\Measurement\Formatter\MeasurementFormatterPluginManagerInterface $plugin_manager
   *   The measurement formatter plugin manager.
   */
  public function __construct(MeasurementFormatterPluginManagerInterface $plugin_manager) {
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.measurement.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function format(MeasurementInterface $measurement, $format = '', $options = []) {
    if ($formatter = $this->getFormatter($format)) {
      return $formatter->format($measurement, $format, $options);
    }

    throw new \Exception("The measurement format '$format' does not exist.");
  }

  /**
   * Gets the formatter for the given format.
   *
   * @param string $format
   *   The format name.
   *
   * @return \Drupal\measurement\Utility\MeasurementFormatterInterface
   *   The measurement formatter.
   */
  protected function getFormatter($format) {
    return $this->pluginManager->getInstance(['id' => $format]);
  }

}
