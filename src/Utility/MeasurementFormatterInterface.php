<?php

namespace Drupal\measurement\Utility;

use Drupal\measurement\Type\MeasurementInterface;

/**
 * Defines a common interface for measurement formatters.
 */
interface MeasurementFormatterInterface {

  /**
   * Formats a measurement in the given format.
   *
   * @param \Drupal\measurement\Type\MeasurementInterface $measurement
   *   The measurement object.
   * @param string $format
   *   (optional) The format to use.
   * @param array $options
   *   (optional) The formatting options.
   *
   * @return string
   *   The formatted measurement.
   *
   * @throws \Exception
   *   Thrown if there was a problem with formatting.
   */
  public function format(MeasurementInterface $measurement, $format = '', $options = []);

}
