<?php

namespace Drupal\measurement\Type;

use Drupal\measurement\Entity\MeasurementUnitInterface;

/**
 * Provides a default implementation of a measurement.
 */
class Measurement implements MeasurementInterface {

  /**
   * The magnitude of the measurement.
   *
   * @var float
   */
  protected $magnitude;

  /**
   * The unit of measurement.
   *
   * @var \Drupal\measurement\Entity\MeasurementUnitInterface
   */
  protected $unit;

  /**
   * Constructs a new Measurement.
   *
   * @param float $magnitude
   *   The magnitude of the measurement.
   * @param \Drupal\measurement\Entity\MeasurementUnitInterface $unit
   *   (optional) The unit of measurement.
   */
  public function __construct($magnitude, MeasurementUnitInterface $unit = NULL) {
    $this->magnitude = $magnitude;
    $this->unit = $unit;
  }

  /**
   * {@inheritdoc}
   */
  public function getMagnitude() {
    return $this->magnitude;
  }

  /**
   * {@inheritdoc}
   */
  public function setMagnitude($magnitude) {
    $this->magnitude = $magnitude;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUnit() {
    return $this->unit;
  }

  /**
   * {@inheritdoc}
   */
  public function setUnit(MeasurementUnitInterface $unit) {
    $this->unit = $unit;

    return $this;
  }

}
