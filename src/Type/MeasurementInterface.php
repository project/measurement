<?php

namespace Drupal\measurement\Type;

use Drupal\measurement\Entity\MeasurementUnitInterface;

/**
 * Defines a common interface for measurements.
 */
interface MeasurementInterface {

  /**
   * Gets the magnitude of the measurement.
   *
   * @return float
   *   The magnitude of the measurement.
   */
  public function getMagnitude();

  /**
   * Sets the magnitude of the measurement.
   *
   * @param float $magnitude
   *   The magnitude of the measurement.
   *
   * @return $this
   */
  public function setMagnitude($magnitude);

  /**
   * Gets the unit of measurement.
   *
   * @return \Drupal\measurement\Entity\MeasurementUnitInterface|null
   *   The unit of measurement, or NULL if it has not been set.
   */
  public function getUnit();

  /**
   * Sets the unit of measurement.
   *
   * @param \Drupal\measurement\Entity\MeasurementUnitInterface $unit
   *   The unit of measurement.
   *
   * @return $this
   */
  public function setUnit(MeasurementUnitInterface $unit);

}
