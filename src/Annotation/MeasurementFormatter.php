<?php

namespace Drupal\measurement\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the measurement formatter plugin annotation.
 *
 * @Annotation
 */
class MeasurementFormatter extends Plugin {

  /**
   * The machine-readable name of the plugin.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The administrative description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
